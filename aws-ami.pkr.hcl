# Packer plugins

packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}


#Ami name for local values 

locals {
  my_ami_name = "my-ami-${formatdate("YYYY-MM-DD-hhmmss", timestamp())}"
}

source "amazon-ebs" "ubuntu" {
  ami_name      = local.my_ami_name
  instance_type = "t2.micro"
  profile       = "Default"
  region        = "eu-west-1"
  ssh_pty       = true
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-focal-20.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]

  }
  ssh_username = "ubuntu"
}

build {
  name    = "packer"
  sources = ["source.amazon-ebs.ubuntu"]

  post-processor "manifest" {
    output     = "manifest.json"
    strip_path = true
    custom_data = {
      version = local.my_ami_name
    }
  }

  provisioner "shell" {
    environment_vars = [
      "name=test"
    ]
    inline = [
      "sudo apt-get install nginx -y",
      "touch file.txt",
      "touch exmple.txt"

    ]
  }

  provisioner "file" {
    destination = "/tmp/nginx.html"
    source      = "./nginx.html"
  }

}